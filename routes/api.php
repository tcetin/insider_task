<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SimulatorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/', [HomeController::class, 'getStarting']);
Route::get('/reset-all', [HomeController::class, 'resetAll']);
Route::get('/standings', [HomeController::class, 'getStandings']);
Route::get('/fixtures', [HomeController::class, 'getFixtures']);


Route::get('/play-all-weeks', [SimulatorController::class, 'playAllWeeks']);
Route::get('/play-week/{weekId}', [SimulatorController::class, 'playWeekly']);
