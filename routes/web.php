<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\SimulatorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/app', [PagesController::class,'index']);

Route::get('/', [HomeController::class,'getStarting']);
Route::get('/reset-all', [HomeController::class,'resetAll']);
Route::get('/standings',  [HomeController::class,'getStandings']);
Route::get('/fixtures', [HomeController::class,'getFixtures']);


Route::get('/play-all-weeks', [SimulatorController::class,'playAllWeeks']);
Route::get('/play-week/{weekId}', [SimulatorController::class,'playWeekly']);
