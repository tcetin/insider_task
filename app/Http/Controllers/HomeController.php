<?php

namespace App\Http\Controllers;

use App\Models\MatchModel;
use App\Models\Team;
use App\Models\Week;
use App\Repositories\LeagueRepository;
use App\Repositories\MatchRepository;
use App\Services\FixtureDraw\HomeAndAwayDraw;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $leagueRepository;
    private $matchRepository;

    public function __construct(LeagueRepository $leagueRepository, MatchRepository $matchRepository)
    {
        $this->leagueRepository = $leagueRepository;
        $this->matchRepository = $matchRepository;
        $this->handleRequirements();
    }

    public function handleRequirements()
    {
        //check if there is no standing yet, make it
        if (!$this->leagueRepository->checkStanding()) {
            $this->leagueRepository->createStanding();
        }
        //check if all matches are drawn
        if (!$this->matchRepository->checkIfFixturesDrawn()) {
            $this->makeFixtures();
        }
    }

    public function getStarting()
    {
        $matches = $this->matchRepository->getFixture()->groupBy('week_id');
        return response()->json([
            'leagues' => $this->leagueRepository->getAll(),
            'weeks' => $this->matchRepository->getWeeks(),
            'matches' => $matches,
        ], 200);

    }

    public function makeFixtures()
    {
        $drawService = new HomeAndAwayDraw($this->matchRepository->getTeamsId());
        $this->matchRepository->createFixture($drawService->getFixturesPlan());
    }

    public function resetAll()
    {
        $this->matchRepository->truncateMatches();
        $this->leagueRepository->truncateLeague();
        $this->makeFixtures();
        return response()->json(['status' => 'ok'], 200);
    }

    public function getLeagues()
    {
        return response()->json($this->leagueRepository->getAll());
    }

    public function getFixtures()
    {
        $weeks = $this->matchRepository->getWeeks();
        $fixture = $this->matchRepository->getFixture()->groupBy('week_id');
        return response()->json(['weeks' => $weeks, 'items' => $fixture]);
    }
}
