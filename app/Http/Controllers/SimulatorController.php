<?php

namespace App\Http\Controllers;

use App\Repositories\LeagueRepository;
use App\Repositories\MatchRepository;
use App\Services\Simulator\MatchSimulator;
use Illuminate\Http\Request;

class SimulatorController extends Controller
{
    private $leagueRepository;
    private $matchRepository;

    public function __construct(LeagueRepository $leagueRepository, MatchRepository $matchRepository)
    {
        $this->leagueRepository = $leagueRepository;
        $this->matchRepository    = $matchRepository;
    }

    public function playAllWeeks()
    {
        $matches = $this->matchRepository->getAllMatches();
        (new MatchSimulator($this->leagueRepository, $this->matchRepository))->bulkSimulate($matches);
        return response()->json(['status' => 'ok'], 200);
    }


    public function playWeekly($week)
    {
        $matches = $this->matchRepository->getMatchesFromWeek($week);
        (new MatchSimulator($this->leagueRepository, $this->matchRepository))->bulkSimulate($matches);
        $result = $this->matchRepository->getFixtureByWeekId($week);

        return response()->json([
            'status' => 'ok',
            'matches' => $result
        ], 201);
    }
}
