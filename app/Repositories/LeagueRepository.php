<?php


namespace App\Repositories;


use App\Models\LeagueTable;
use App\Models\Team;

class LeagueRepository
{
    private $league;
    private $team;

    public function __construct(LeagueTable $league, Team $team)
    {
        $this->league = $league;
        $this->team = $team;
    }

    public function checkStanding()
    {
        $result = $this->league->get();
        return $result->isEmpty() ? false : true;
    }

    public function createStanding()
    {
        $result = $this->league->get();
        if (!$result->isEmpty()) {
            return;
        }
        foreach ($this->getTeams() as $value) {
            $data = [
                'team_id'    => $value,
                'played'     => 0,
                'won'        => 0,
                'lose'       => 0,
                'draw'       => 0,
                'goal_drawn' => 0,
                'points'     => 0
            ];
            $this->league->create($data);
        }

    }

    public function getTeams()
    {
        return $this->team->pluck('id');
    }

    public function getAll()
    {
        return $this->team->leftJoin('league_tables', 'teams.id', '=', 'league_tables.team_id')
            ->orderBy('league_tables.points', 'DESC')
            ->orderBy('league_tables.goal_drawn', 'DESC')
            ->orderBy('league_tables.won', 'DESC')
            ->get();
    }

    public function getLeagueByTeamId($team_id)
    {
        return $this->league->where('team_id', $team_id)->first();
    }

    public function truncateLeague()
    {
        $this->league->truncate();
    }

    public function checkLeagueStatus()
    {
        return $this->league->select('played')->first();
    }
}
