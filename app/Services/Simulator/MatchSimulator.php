<?php


namespace App\Services\Simulator;

use App\Repositories\MatchRepository;
use App\Repositories\LeagueRepository;

class MatchSimulator implements ResultSimulatorInterface
{

    protected $leagueRepository;
    protected $matchRepository;

    public function __construct(LeagueRepository $leagueRepository, MatchRepository $matchRepository)
    {
        $this->leagueRepository = $leagueRepository;
        $this->matchRepository = $matchRepository;
    }

    public function simulate($match)
    {
        $home = $this->leagueRepository->getLeagueByTeamId($match->home_team);
        $away = $this->leagueRepository->getLeagueByTeamId($match->away_team);
        $homeScore = $this->generateScore(true, $home->id);
        $awayScore = $this->generateScore(false, $away->id);

        $this->updateMatchScore($homeScore, $awayScore, $home, $away);
        return $this->matchRepository->resultSaver($match, $homeScore, $awayScore);

    }

    public function bulkSimulate($matches)
    {
        foreach ($matches as $match) {
            $this->simulate($match);
        }
    }

    public function updateMatchScore($homeScore, $awayScore, $home, $away)
    {
        $this->matchRepository->updateMatchScore($homeScore, $awayScore, $home, $away);
    }


    public function generateScore(bool $is_home, int $teamRank)
    {
        //this generator is assuming home team and also current rank to generate result
        return $is_home ? rand(0, 10) : rand(0, 10 - $teamRank);
    }
}

