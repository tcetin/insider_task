<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            ['name' => 'Team-1', 'strength' => rand(1,10)],
            ['name' => 'Team-2', 'strength' => rand(1,10)],
            ['name' => 'Team-3', 'strength' => rand(1,10)],
            ['name' => 'Team-4', 'strength' => rand(1,10)],
        ]);
    }
}
